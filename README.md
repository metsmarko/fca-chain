The aim of this project is to improve readability of concept lattices which are used in formal concept analysis (FCA).
FCA is a method for data analysis. Even small datasets can generate huge concept lattices with thousands of concepts
and which are impossible for humans to analyze visually. There are many different approaches to reducing the amount of
concepts in the lattice but there are now standard way of doing it. Therefore this project proposes one more way of 
reducing the complexity of the concept lattice. Concept reduction is achieved by finding the concept chains from the 
concept lattice. Concept chain is a set of structurally ordered concepts from top of the concept lattice to the bottom
of the concept lattice. Analysis of the proposed method shows that it is possible to reduce the amount of concepts 
drastically while not losing too much in original data.

### Installing dependencies
From root directory do the following:
* Create virtual environment:  
 `virtualenv venv`
* Activate virtual environment:  
 `source venv/bin/activate`
* Install dependencies:  
 `pip install -r requirements.txt`

### Running
`python main.py --data_file=... --amount_of_chains=... [--method=...]`

Mandatory parameters:

--data_file - CSV file containing the formal context. Relations can be defined in two ways:
* by using 1 when relation is present and 0 when no relation is present
* by using X when relation is present and empty string when no relation is present

--amount_of_chains - amount of chains to be found

Optional parameters

--method - specifies which method to use when ordering objects, possible values are:
* mf - will use minus frame (default)
* freq - will use conformity scale

### Example datasets
Some example datasets can be found in data directory