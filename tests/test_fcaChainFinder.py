from fca_chain.FcaChainFinder import FcaChainFinder
from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.clustering.HierarchicalClustering import HierarchicalClustering
from fca_chain.concept.ConceptCreator import ConceptCreator
from tests.TestBase import TestBase


class TestFcaChainFinder(TestBase):
    def setUp(self):
        context = self.make_people_context()
        self.clustering = HierarchicalClustering()
        df = context.get_context()
        self.chain_finder = FcaChainFinder(context)
        cc = ConceptCreator(context)
        self.chain1 = ConceptChain()
        concepts = [
            cc.create_concept([df.index.get_loc(n) for n in ['girl', 'woman']],
                              [df.columns.get_loc(n) for n in ['female']]),
            cc.create_concept([df.index.get_loc(n) for n in ['woman']],
                              [df.columns.get_loc(n) for n in ['female', 'adult']]),
            cc.create_concept([],
                              [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        ]
        self.chain1.append_concepts(concepts)

        self.chain2 = ConceptChain()
        concepts = [
            cc.create_concept([df.index.get_loc(n) for n in ['girl', 'boy']],
                              [df.columns.get_loc(n) for n in ['juvenile']]),
            cc.create_concept([df.index.get_loc(n) for n in ['boy']],
                              [df.columns.get_loc(n) for n in ['male', 'juvenile']]),
            cc.create_concept([],
                              [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        ]
        self.chain2.append_concepts(concepts)

        self.chain3 = ConceptChain()
        concepts = [
            cc.create_concept([df.index.get_loc(n) for n in ['woman', 'man']],
                              [df.columns.get_loc(n) for n in ['adult']]),
            cc.create_concept([df.index.get_loc(n) for n in ['man']],
                              [df.columns.get_loc(n) for n in ['male', 'adult']]),
            cc.create_concept([],
                              [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        ]
        self.chain3.append_concepts(concepts)

        self.chain4 = ConceptChain()
        concepts = [
            cc.create_concept([df.index.get_loc(n) for n in ['boy', 'man']],
                              [df.columns.get_loc(n) for n in ['male']]),
            cc.create_concept([df.index.get_loc(n) for n in ['man']],
                              [df.columns.get_loc(n) for n in ['male', 'adult']]),
            cc.create_concept([],
                              [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        ]
        self.chain4.append_concepts(concepts)

        self.chain5 = ConceptChain()
        concepts = [
            cc.create_concept([df.index.get_loc(n) for n in ['woman', 'man']],
                              [df.columns.get_loc(n) for n in ['adult']]),
            cc.create_concept([df.index.get_loc(n) for n in ['woman']],
                              [df.columns.get_loc(n) for n in ['female', 'adult']]),
            cc.create_concept([],
                              [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        ]
        self.chain5.append_concepts(concepts)

        self.chain6 = ConceptChain()
        concepts = [
            cc.create_concept([df.index.get_loc(n) for n in ['boy', 'man']],
                              [df.columns.get_loc(n) for n in ['male']]),
            cc.create_concept([df.index.get_loc(n) for n in ['boy']],
                              [df.columns.get_loc(n) for n in ['juvenile', 'male']]),
            cc.create_concept([],
                              [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        ]
        self.chain6.append_concepts(concepts)

    def test_find_chain_mf(self):
        chains = self.chain_finder.find_chain(1, self.clustering)
        self.assertEqual(len(chains), 1)
        self.assertEqual(chains[0], self.chain3)

        chains = self.chain_finder.find_chain(3, self.clustering)
        self.assertEqual(len(chains), 3)
        self.assertEqual(set(chains), {self.chain1, self.chain2, self.chain3})

    def test_find_chain_freq(self):
        chains = self.chain_finder.find_chain(1, self.clustering, 'freq')
        self.assertEqual(len(chains), 1)
        self.assertEqual(chains[0], self.chain4)

        chains = self.chain_finder.find_chain(3, self.clustering, 'freq')
        self.assertEqual(len(chains), 3)
        self.assertEqual(set(chains), {self.chain4, self.chain5, self.chain6})

    def test_find_chain_wrong_method(self):
        try:
            self.chain_finder.find_chain(1, self.clustering, 'blah')
            self.fail()
        except Exception as e:
            self.assertEqual(str(e), "Unknown chain finding method")

    def test_calculate_chain_cover(self):
        self.assertEqual(self.chain_finder.calculate_chains_cover([]), 0)
        self.assertEqual(self.chain_finder.calculate_chains_cover([self.chain1]), 0.375)
        self.assertEqual(self.chain_finder.calculate_chains_cover([self.chain1, self.chain1]), 0.375)
        self.assertEqual(self.chain_finder.calculate_chains_cover([self.chain1, self.chain2]), 0.75)
        self.assertEqual(self.chain_finder.calculate_chains_cover([self.chain1, self.chain2, self.chain3]), 1.0)
