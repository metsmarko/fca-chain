import os
from unittest import TestCase

from pandas import DataFrame, np

from fca_chain.data.DataReader import DataReader
from fca_chain.model.Context import Context


class TestBase(TestCase):
    # Context taken from here:
    # http://www.celta.paris-sorbonne.fr/anasem/Semana-Corner/FCAexample.jpg
    def make_people_context(self) -> Context:
        df = DataFrame(np.array([[1, 1, 0, 0],
                                 [1, 0, 1, 0],
                                 [0, 1, 0, 1],
                                 [0, 0, 1, 1]]))

        df.columns = ['female', 'juvenile', 'adult', 'male']
        df.index = ['girl', 'woman', 'boy', 'man']
        return Context(df)

    def make_water_bodies_context(self):
        test_root_dir = os.path.dirname(os.path.abspath(__file__))
        return DataReader.read_context_form_file(test_root_dir + "/bodies_of_water.csv")
