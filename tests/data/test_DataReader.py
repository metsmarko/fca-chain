import os
from unittest import TestCase

from fca_chain.data.DataReader import DataReader


class TestDataReader(TestCase):
    TEST_ROOT_DIR = os.path.dirname(os.path.abspath(__file__))

    def test_read_context_form_file(self):
        self.assert_context_values("/test_context_with_x.csv")
        self.assert_context_values("/test_context.csv")

    def assert_context_values(self, csv_file):
        context = DataReader.read_context_form_file(self.TEST_ROOT_DIR + csv_file)
        self.assertEqual(list(context.get_context_values()[0]), [1, 1, 0, 1, 0])
        self.assertEqual(list(context.get_context_values()[1]), [0, 1, 1, 0, 0])
        self.assertEqual(list(context.get_context_values()[2]), [0, 1, 0, 1, 0])
        self.assertEqual(list(context.get_context_values()[3]), [1, 0, 0, 0, 0])
        self.assertEqual((4, 5), context.get_context_values().shape)
