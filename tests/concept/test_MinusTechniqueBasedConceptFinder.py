from fca_chain.concept.ConceptCreator import ConceptCreator
from fca_chain.concept.MinusTechniqueBasedConceptFinder import MinusTechniqueBasedConceptFinder
from tests.TestBase import TestBase


class TestMinusTechniqueBasedConceptFinder(TestBase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.context = self.make_water_bodies_context()
        self.cc = ConceptCreator(self.context)
        self.finder = MinusTechniqueBasedConceptFinder(self.context, self.cc)

    def test_next_concepts_start_from_bottom(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea']],
                                                  [df.columns.get_loc(n) for n in ['natural', 'stagnant',
                                                                                   'constant', 'maritime']])

        self.assertEqual(self.finder.next_concepts(starting_concept),
                         [self.cc.create_concept([], [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant',
                                                                                      'maritime', 'running',
                                                                                      'temporary']])])

    def test_prev_concepts_start_from_bottom(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ('lagoon', 'sea')],
                                                  [df.columns.get_loc(n) for n in
                                                   ('natural', 'stagnant', 'constant', 'maritime')])

        concept1 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn', 'lake',
                                                                         'maar', 'puddle', 'pond', 'pool', 'torrent',
                                                                         'river', 'rivulet', 'runnel', 'stream']],
                                          [df.columns.get_loc(n) for n in ['natural']])
        concept2 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn', 'lake',
                                                                         'maar', 'pond', 'pool', 'torrent', 'river',
                                                                         'rivulet', 'runnel', 'stream']],
                                          [df.columns.get_loc(n) for n in ['natural', 'constant']])
        concept3 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'tarn', 'lake',
                                                                         'maar', 'pond', 'pool']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant']])
        expected_chain = [concept1, concept2, concept3]
        self.assertEqual(self.finder.prev_concepts(starting_concept), expected_chain)

    def test_next_concepts_start_from_middle(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                                 'lake', 'maar', 'pond', 'pool',
                                                                                 'torrent', 'river', 'rivulet',
                                                                                 'runnel', 'stream']],
                                                  [df.columns.get_loc(n) for n in ['natural', 'constant']])

        concept1 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'tarn', 'lake',
                                                                         'maar', 'pond', 'pool']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant']])
        concept2 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant',
                                                                           'constant', 'maritime']])
        concept3 = self.cc.create_concept([],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant',
                                                                           'maritime', 'running', 'temporary']])

        self.assertEqual(self.finder.next_concepts(starting_concept), [concept1, concept2, concept3])

    def test_prev_concepts_start_from_middle(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                                 'lake', 'maar', 'pond', 'pool',
                                                                                 'torrent', 'river', 'rivulet',
                                                                                 'runnel', 'stream']],
                                                  [df.columns.get_loc(n) for n in ['natural', 'constant']])

        concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                        'lake', 'maar', 'pond', 'pool',
                                                                        'torrent', 'river', 'rivulet',
                                                                        'runnel', 'stream', 'puddle']],
                                         [df.columns.get_loc(n) for n in ['natural']])
        self.assertEqual(self.finder.prev_concepts(starting_concept), [concept])

    def test_next_concepts_start_from_top(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['canal', 'channel', 'lagoon', 'lake',
                                                                                 'maar', 'puddle', 'pond', 'pool',
                                                                                 'reservoir', 'river', 'rivulet',
                                                                                 'runnel', 'sea', 'stream', 'tarn',
                                                                                 'torrent', 'trickle']], [])

        concept1 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                         'lake', 'maar', 'pond', 'pool',
                                                                         'torrent', 'river', 'rivulet',
                                                                         'runnel', 'stream', 'canal', 'channel',
                                                                         'reservoir']],
                                          [df.columns.get_loc(n) for n in ['constant']])
        concept2 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                         'lake', 'maar', 'pond', 'pool',
                                                                         'torrent', 'river', 'rivulet',
                                                                         'runnel', 'stream']],
                                          [df.columns.get_loc(n) for n in ['natural', 'constant']])
        concept3 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'tarn', 'lake',
                                                                         'maar', 'pond', 'pool']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant']])
        concept4 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant',
                                                                           'constant', 'maritime']])
        concept5 = self.cc.create_concept([], [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant',
                                                                               'maritime', 'running',
                                                                               'temporary']])
        concepts = self.finder.next_concepts(starting_concept)
        self.assertEqual(concepts, [concept1, concept2, concept3, concept4, concept5])

    def test_prev_concepts_start_from_top(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['canal', 'channel', 'lagoon', 'lake',
                                                                                 'maar', 'puddle', 'pond', 'pool',
                                                                                 'reservoir', 'river', 'rivulet',
                                                                                 'runnel', 'sea', 'stream', 'tarn',
                                                                                 'torrent', 'trickle']], [])
        self.assertEqual(self.finder.prev_concepts(starting_concept), [])
