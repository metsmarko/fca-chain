from fca_chain.concept.ConceptCreator import ConceptCreator
from fca_chain.concept.FrequencyBasedConceptFinder import FrequencyBasedConceptFinder
from tests.TestBase import TestBase


class TestFrequencyBasedConceptFinder(TestBase):
    def __init__(self, methodName='runTest'):
        super().__init__(methodName)
        self.context = self.make_water_bodies_context()
        self.cc = ConceptCreator(self.context)
        self.finder = FrequencyBasedConceptFinder(self.context, self.cc)

    def test_next_concepts(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                                 'lake', 'maar', 'pond', 'pool',
                                                                                 'torrent', 'river', 'rivulet',
                                                                                 'runnel', 'stream']],
                                                  [df.columns.get_loc(n) for n in ['natural', 'constant']])
        concept1 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'tarn', 'lake',
                                                                         'maar', 'pond', 'pool']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant']])
        concept2 = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea']],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant',
                                                                           'constant', 'maritime']])
        concept3 = self.cc.create_concept([],
                                          [df.columns.get_loc(n) for n in ['natural', 'stagnant', 'constant',
                                                                           'maritime', 'running', 'temporary']])

        concepts = self.finder.next_concepts(starting_concept)
        self.assertEqual(concepts, [concept1, concept2, concept3])

    def test_prev_concepts(self):
        df = self.context.get_context()
        starting_concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn',
                                                                                 'lake', 'maar', 'pond', 'pool',
                                                                                 'torrent', 'river', 'rivulet',
                                                                                 'runnel', 'stream']],
                                                  [df.columns.get_loc(n) for n in ['natural', 'constant']])
        concept = self.cc.create_concept([df.index.get_loc(n) for n in ['lagoon', 'sea', 'trickle', 'tarn', 'lake',
                                                                        'maar', 'pond', 'pool', 'torrent',
                                                                        'river', 'rivulet', 'runnel', 'stream',
                                                                        'channel', 'canal', 'reservoir']],
                                         [df.columns.get_loc(n) for n in ['constant']])
        self.assertEqual(self.finder.prev_concepts(starting_concept), [concept])
