from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.concept.ConceptCreator import ConceptCreator
from fca_chain.visual.Lattice import Lattice
from tests.TestBase import TestBase


class LatticeTest(TestBase):
    def test_create_lattice_from_empty_concepts(self):
        lattice = Lattice.create_lattice_from_concepts([])
        self.assertEqual(lattice.get_lattice_nodes(), [])

    def test_create_lattice_from_concepts(self):
        context = self.make_people_context()
        cc = ConceptCreator(context)
        df = context.get_context()
        concept_1 = cc.create_concept([],
                                      [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']])
        concept_2 = cc.create_concept([df.index.get_loc(n) for n in ['man']],
                                      [df.columns.get_loc(n) for n in ['adult', 'male']])
        concept_3 = cc.create_concept([df.index.get_loc(n) for n in ['boy']],
                                      [df.columns.get_loc(n) for n in ['juvenile', 'male']])
        concept_4 = cc.create_concept([df.index.get_loc(n) for n in ['boy', 'man']],
                                      [df.columns.get_loc(n) for n in ['male']])
        concept_5 = cc.create_concept([df.index.get_loc(n) for n in ['woman']],
                                      [df.columns.get_loc(n) for n in ['female', 'adult']])
        concept_6 = cc.create_concept([df.index.get_loc(n) for n in ['woman', 'man']],
                                      [df.columns.get_loc(n) for n in ['adult']])
        concept_7 = cc.create_concept([df.index.get_loc(n) for n in ['girl']],
                                      [df.columns.get_loc(n) for n in ['female', 'juvenile']])
        concept_8 = cc.create_concept([df.index.get_loc(n) for n in ['girl', 'boy']],
                                      [df.columns.get_loc(n) for n in ['juvenile']])
        concept_9 = cc.create_concept([df.index.get_loc(n) for n in ['girl', 'woman']],
                                      [df.columns.get_loc(n) for n in ['female']])
        concept_10 = cc.create_concept([df.index.get_loc(n) for n in ['girl', 'woman', 'boy', 'man']],
                                      [])
        concepts = [concept_1,
                    concept_2,
                    concept_3,
                    concept_4,
                    concept_5,
                    concept_6,
                    concept_7,
                    concept_8,
                    concept_9,
                    concept_10
                    ]
        lattice = Lattice.create_lattice_from_concepts(concepts)
        lattice_nodes = lattice.get_lattice_nodes()

        self.assertEqual(len(lattice_nodes), 10)

        self.assertEqual(lattice_nodes[0].get_concept(), concept_10)
        self.assertEqual(lattice_nodes[0].get_attributes(), frozenset())
        self.assertEqual(lattice_nodes[0].get_objects(), frozenset())
        self.assertEqual(set(n.get_concept() for n in lattice_nodes[0].get_next_nodes()),
                         {concept_8, concept_4, concept_9, concept_6})

        # first row
        first_row_concepts = [concept_4, concept_6, concept_8, concept_9]
        self.assertIn(lattice_nodes[1].get_concept(), first_row_concepts)
        self.assertIn(lattice_nodes[2].get_concept(), first_row_concepts)
        self.assertIn(lattice_nodes[3].get_concept(), first_row_concepts)
        self.assertIn(lattice_nodes[4].get_concept(), first_row_concepts)
        self.assertEqual(len(lattice_nodes[1].get_next_nodes()), 2)
        self.assertEqual(len(lattice_nodes[2].get_next_nodes()), 2)
        self.assertEqual(len(lattice_nodes[3].get_next_nodes()), 2)
        self.assertEqual(len(lattice_nodes[4].get_next_nodes()), 2)

        # second row
        second_row_concepts = [concept_2, concept_3, concept_5, concept_7]
        self.assertIn(lattice_nodes[5].get_concept(), second_row_concepts)
        self.assertIn(lattice_nodes[6].get_concept(), second_row_concepts)
        self.assertIn(lattice_nodes[7].get_concept(), second_row_concepts)
        self.assertIn(lattice_nodes[8].get_concept(), second_row_concepts)
        self.assertEqual(len(lattice_nodes[5].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[6].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[7].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[8].get_next_nodes()), 1)

        self.assertEqual(lattice_nodes[9].get_concept(), concept_1)
        self.assertEqual(lattice_nodes[9].get_attributes(), frozenset())
        self.assertEqual(lattice_nodes[9].get_objects(), frozenset())
        self.assertEqual(len(lattice_nodes[9].get_next_nodes()), 0)

    def test_create_lattice_from_chains(self):
        context = self.make_people_context()
        df = context.get_context()
        cc = ConceptCreator(context)
        chain1 = ConceptChain()
        chain1.append_concept(cc.create_concept([df.index.get_loc(n) for n in ['girl', 'woman']],
                                                [df.columns.get_loc(n) for n in ['female']]))
        chain1.append_concept(cc.create_concept([df.index.get_loc(n) for n in ['girl']],
                                                [df.columns.get_loc(n) for n in ['female', 'juvenile']]))
        chain1.append_concept(cc.create_concept([],
                                                [df.columns.get_loc(n) for n in ['female', 'juvenile', 'adult', 'male']]))
        chain2 = chain1
        lattice = Lattice.create_lattice_from_chains([chain1, chain2])
        lattice_nodes = lattice.get_lattice_nodes()

        self.assertEqual(len(lattice_nodes), 6)
        self.assertEqual(lattice_nodes[0].get_concept(), chain1.get_concepts()[0])
        self.assertEqual(lattice_nodes[1].get_concept(), chain1.get_concepts()[1])
        self.assertEqual(lattice_nodes[2].get_concept(), chain1.get_concepts()[2])
        self.assertEqual(lattice_nodes[3].get_concept(), chain2.get_concepts()[0])
        self.assertEqual(lattice_nodes[4].get_concept(), chain2.get_concepts()[1])
        self.assertEqual(lattice_nodes[5].get_concept(), chain2.get_concepts()[2])

        self.assertEqual(len(lattice_nodes[0].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[1].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[2].get_next_nodes()), 0)
        self.assertEqual(len(lattice_nodes[3].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[4].get_next_nodes()), 1)
        self.assertEqual(len(lattice_nodes[5].get_next_nodes()), 0)

        self.assertEqual(lattice_nodes[0].get_next_nodes()[0].get_concept(), chain1.get_concepts()[1])
        self.assertEqual(lattice_nodes[1].get_next_nodes()[0].get_concept(), chain1.get_concepts()[2])
        self.assertEqual(lattice_nodes[3].get_next_nodes()[0].get_concept(), chain2.get_concepts()[1])
        self.assertEqual(lattice_nodes[4].get_next_nodes()[0].get_concept(), chain2.get_concepts()[2])

    def test_create_chain_from_empty_concepts(self):
        lattice = Lattice.create_lattice_from_chains([])
        self.assertEqual(lattice.get_lattice_nodes(), [])
