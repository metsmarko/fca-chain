from unittest import TestCase

from pandas import DataFrame, np

from fca_chain.model.Context import Context


class ContextTest(TestCase):
    attributes = ['a', 'b', 'c', 'd', 'e']
    objects = ['w', 'x', 'y', 'z']

    @classmethod
    def setUpClass(cls):
        super(ContextTest, cls).setUpClass()
        df = DataFrame(np.array([[0, 1, 1, 0, 0],
                                 [0, 1, 0, 1, 0],
                                 [1, 1, 0, 1, 0],
                                 [1, 0, 0, 0, 0]]))

        df.columns = cls.attributes
        df.index = cls.objects
        cls.context = Context(df)

    def test_attribute_name(self):
        for i in range(len(self.attributes)):
            self.assertEqual(self.context.get_attribute_name(i), self.attributes[i])

    def test_get_context(self):
        self.assertEqual(self.context.get_context().shape, (4, 5))

    def test_get_intent(self):
        self.assertEqual(self.context.get_intent(0), [0, 1, 3])

    def test_get_attribute_indices(self):
        self.assertEqual(self.context.get_attribute_indices(), [0, 1, 2, 3, 4])

    def test_get_object_indices(self):
        self.assertEqual(self.context.get_object_indices(), [0, 1, 2, 3])

    def test_object_name(self):
        self.assertEqual(self.context.get_object_name(0), self.objects[2])
        self.assertEqual(self.context.get_object_name(3), self.objects[3])
