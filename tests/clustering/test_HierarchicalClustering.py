from tests.TestBase import TestBase

from fca_chain.clustering.HierarchicalClustering import HierarchicalClustering


class TestHierarchicalClustering(TestBase):
    def test_cluster_data(self):
        context_values = self.make_people_context().get_context_values()
        hierarchical_clustering = HierarchicalClustering()

        self.assertEqual(hierarchical_clustering.cluster_data(context_values, 1).tolist(), [0, 0, 0, 0])
        self.assertEqual(hierarchical_clustering.cluster_data(context_values, 2).tolist(), [1, 1, 0, 0])
        self.assertEqual(hierarchical_clustering.cluster_data(context_values, 3).tolist(), [0, 0, 2, 1])
        self.assertEqual(hierarchical_clustering.cluster_data(context_values, 4).tolist(), [3, 1, 2, 0])
        try:
            hierarchical_clustering.cluster_data(context_values, 0)
            self.fail()
        except ValueError as e:
            self.assertEqual(str(e),
                             "n_clusters should be an integer greater than 0. 0 was provided.")
        try:
            hierarchical_clustering.cluster_data(context_values, 5)
            self.fail()
        except ValueError as e:
            self.assertEqual(str(e),
                             "Cannot extract more clusters than samples: 5 clusters where given for a tree with 4 leaves.")
