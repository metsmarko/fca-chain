from typing import List

import numpy as np
import pandas as pd

from fca_chain.concept.AbstractConceptFinder import AbstractConceptFinder


class MinusTechniqueBasedConceptFinder(AbstractConceptFinder):
    def _order_objects(self, data: np.ndarray, indices: List[int]) -> List[int]:
        if len(data) == 0:
            return []
        rows, columns = data.shape
        attr_frequencies = self._get_attr_frequencies(data, rows)
        object_frequencies = self._get_object_frequencies(data, attr_frequencies)
        concept_order = self._new_concept_order(object_frequencies, indices)
        remaining_obj = concept_order['index'] < 0
        for i in range(rows):
            min_idx = (concept_order[remaining_obj])['weight'].idxmin()
            remaining_obj[min_idx] = False
            concept_order.loc[min_idx, 'index'] = i
            attr_frequencies -= data[min_idx]
            concept_order['weight'] = self._get_object_frequencies(data, attr_frequencies)
        return list(concept_order.sort_values('index', ascending=False).indices)

    def _new_concept_order(self, weight_v: np.ndarray, indices: List[int]) -> pd.DataFrame:
        return pd.DataFrame({'weight': weight_v, 'indices': indices, 'index': -1})
