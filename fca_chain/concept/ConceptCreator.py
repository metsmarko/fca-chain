from typing import List, Set

import numpy as np
from numpy import ndarray

from fca_chain.model.Attribute import Attribute
from fca_chain.model.Concept import Concept
from fca_chain.model.Context import Context
from fca_chain.model.Object import Object


class ConceptCreator:
    def __init__(self, context: Context) -> None:
        self._context = context

    def create_concept_from_attr(self, attributes: List[int]) -> Concept:
        intent = []
        attribute_indices = []
        for k, v in enumerate(attributes):
            if v == 1:
                intent.append(Attribute(k, self._context.get_attribute_name(k)))
                attribute_indices.append(k)
        objects = self._get_objects_with_attributes(attribute_indices, self._context.get_context_values())
        extent = [Object(i, self._context.get_object_name(i)) for i in objects]
        return Concept(extent, intent)

    def create_concept_from_attr_ids(self, attribute_ids: Set[int]) -> Concept:
        attributes = len(self._context.get_attribute_indices()) * [0]
        for id in list(attribute_ids):
            attributes[id] = 1
        return self.create_concept_from_attr(attributes)

    def create_concept(self, objects: List[int], attributes: List[int]) -> Concept:
        extent = [Object(i, self._context.get_object_name(i)) for i in objects]
        intent = [Attribute(i, self._context.get_attribute_name(i)) for i in attributes]
        return Concept(extent, intent)

    def _get_objects_with_attributes(self, attribute_indices: List[int], data: ndarray) -> list:
        return np.argwhere(np.all(data[:, attribute_indices] == 1, axis=1))[:, 0].tolist()
