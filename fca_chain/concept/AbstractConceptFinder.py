from typing import Tuple, Set, FrozenSet, List

import numpy as np
from abc import ABCMeta, abstractmethod

from fca_chain.concept import ConceptCreator
from fca_chain.model.Concept import Concept
from fca_chain.model.Context import Context


class AbstractConceptFinder(metaclass=ABCMeta):
    def __init__(self, context: Context, concept_creator: ConceptCreator) -> None:
        super().__init__()
        self.concept_creator = concept_creator
        self.context = context

    def next_concepts(self, concept: Concept) -> List[Concept]:
        """
       Finds concepts that come after provided concept

       :param concept: Concept from which to find previous concepts
       :return: list of concepts that come before provided concept
       """
        data, indices = self._get_child_objects(concept.get_intent_ids(), self.context.get_context_values())
        return self._find_next(self._order_objects(data, indices), concept)

    def prev_concepts(self, concept: Concept) -> List[Concept]:
        """
       Finds concepts that come before provided concept

       :param concept: Concept from which to find next concepts
       :return: list of concepts that come after provided concept
       """
        data, indices = self._get_parent_objects(concept.get_intent_ids(), self.context.get_context_values())
        return self._find_prev(self._order_objects(data, indices), concept)

    def _find_next(self, ordered_objects: List[int], concept: Concept) -> List[Concept]:
        starting_extent = set()
        starting_intent = set(self.context.get_attribute_indices())
        concepts = self._find_concepts(concept, ordered_objects, starting_extent, starting_intent)
        return concepts

    def _find_prev(self, ordered_objects: List[int], concept: Concept) -> List[Concept]:
        starting_extent = set(concept.get_extent_ids())
        starting_intent = set(concept.get_intent_ids())
        concepts = self._find_concepts(concept, ordered_objects, starting_extent, starting_intent, False)[:-1]
        return concepts

    def _find_concepts(self, concept: Concept, ordered_objects: List[int], extent: Set[int], intent: Set[int],
                       stop_at_starting_concept: bool = True) -> List[Concept]:
        concepts = []
        for obj in ordered_objects:
            obj_intent = set(self.context.get_intent(obj))
            if stop_at_starting_concept and self._is_starting_concept_reached(obj_intent,
                                                                              set(concept.get_intent_ids())):
                break
            if len(intent - obj_intent) > 0:
                concepts.append(self.concept_creator.create_concept_from_attr_ids(intent))
                extent = extent | {obj}
                intent = intent & obj_intent
            else:
                extent.add(obj)
        if len(intent) > 0 and not (
                stop_at_starting_concept and self._is_starting_concept_reached(intent,
                                                                               set(concept.get_intent_ids()))):
            concepts.append(self.concept_creator.create_concept(extent, intent))
        concepts.reverse()
        return concepts

    @abstractmethod
    def _order_objects(self, data: np.ndarray, indices: List[int]) -> List[int]:
        pass

    def _get_object_frequencies(self, data: np.ndarray, attr_frequencies: np.ndarray) -> np.ndarray:
        return np.dot(data, attr_frequencies)

    def _get_attr_frequencies(self, data: np.ndarray, rows: int) -> np.ndarray:
        return np.dot(np.ones(rows), data)

    def _get_child_objects(self, attribute_indices: FrozenSet[int], data: np.ndarray) -> Tuple[np.ndarray, List[int]]:
        indices = np.argwhere(np.all(data[:, list(attribute_indices)], axis=1))[:, 0].tolist()
        return data[indices], indices

    def _get_parent_objects(self, attribute_indices: FrozenSet[int], data: np.ndarray) -> Tuple[np.ndarray, List[int]]:
        indices = list(reversed(np.argwhere(np.any(data[:, list(attribute_indices)], axis=1))[:, 0].tolist()))
        return data[indices], indices

    def _is_starting_concept_reached(self, current_intent: Set[int], target_intent: Set[int]):
        return current_intent == target_intent
