from typing import List

import numpy as np
import pandas as pd

from fca_chain.concept import ConceptCreator
from fca_chain.concept.AbstractConceptFinder import AbstractConceptFinder
from fca_chain.model.Context import Context


class FrequencyBasedConceptFinder(AbstractConceptFinder):
    def __init__(self, context: Context, concept_creator: ConceptCreator) -> None:
        super().__init__(context, concept_creator)

    def _order_objects(self, data: np.ndarray, indices: List[int]) -> List[int]:
        if len(data) == 0:
            return []
        rows, columns = data.shape
        attr_frequencies = self._get_attr_frequencies(data, rows)
        object_frequencies = self._get_object_frequencies(data, attr_frequencies)
        concept_order = pd.DataFrame({'weight': object_frequencies, 'indices': indices})
        return list(concept_order.sort_values(['weight'], ascending=False).indices)
