from typing import List, Dict

from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.model.Concept import Concept
from fca_chain.visual.LatticeNode import LatticeNode


class Lattice:
    lattice_nodes = []

    def __init__(self, lattice_nodes) -> None:
        super().__init__()
        self.lattice_nodes = lattice_nodes

    def get_lattice_nodes(self) -> List[LatticeNode]:
        return self.lattice_nodes

    @classmethod
    def create_lattice_from_chains(cls, concept_chains: List[ConceptChain]) -> 'Lattice':
        nodes = []
        for concept_chain in concept_chains:
            prev_node = None
            chain_nodes = []
            for concept in concept_chain.get_concepts():
                node = LatticeNode(concept)
                if prev_node:
                    prev_node.add_next_node(node)
                prev_node = node
                chain_nodes.append(node)
            cls._add_attributes_to_nodes(chain_nodes)
            cls._add_objects_to_nodes(chain_nodes)
            nodes.extend(chain_nodes)
        return Lattice(nodes)

    @classmethod
    def create_lattice_from_concepts(cls, concepts: List[Concept]) -> 'Lattice':
        lattice_nodes = cls._make_lattice_nodes(concepts)
        cls._add_attributes_to_nodes(lattice_nodes)
        cls._add_objects_to_nodes(lattice_nodes)
        return Lattice(lattice_nodes)

    @classmethod
    def _make_lattice_nodes(cls, concepts: List[Concept]):
        concepts = sorted(list(set(concepts)), key=lambda x: len(x.get_intent()), reverse=True)
        concepts_to_lattice_nodes = {}
        for i, c in enumerate(concepts):
            child_node = cls._get_node(c, concepts_to_lattice_nodes)
            for j in range(i + 1, len(concepts)):
                c2 = concepts[j]
                if len(c.get_intent()) - len(c2.get_intent()) > 1:
                    break
                elif len(c.get_intent()) - len(c2.get_intent()) == 1 and \
                        len(set(c.get_intent_ids()) - set(c2.get_intent_ids())) == 1:
                    parent_node = cls._get_node(c2, concepts_to_lattice_nodes)
                    parent_node.add_next_node(child_node)
        lattice_nodes = []
        concepts.reverse()
        for concept in concepts:
            node = concepts_to_lattice_nodes[concept]
            if not node.get_next_nodes():
                last_node = concepts_to_lattice_nodes[concepts[-1]]
                if last_node != node:
                    node.add_next_node(last_node)
            lattice_nodes.append(node)
        return lattice_nodes

    @classmethod
    def _add_attributes_to_nodes(cls, lattice_nodes):
        seen_intents = set()
        for node in lattice_nodes:
            intent = set([attribute.get_name() for attribute in node.get_concept().get_intent()])
            new_intents = intent - seen_intents
            seen_intents = intent | seen_intents
            node.set_attributes(new_intents)

    @classmethod
    def _add_objects_to_nodes(cls, lattice_nodes):
        seen_extents = set()
        for node in reversed(lattice_nodes):
            extent = set([obj.get_name() for obj in node.get_concept().get_extent()])
            new_extents = extent - seen_extents
            seen_extents = extent | seen_extents
            node.set_objects(new_extents)

    @classmethod
    def _get_node(cls, concept: Concept, concepts_to_lattice_nodes: Dict[Concept, LatticeNode]) -> LatticeNode:
        if concept not in concepts_to_lattice_nodes:
            concepts_to_lattice_nodes[concept] = LatticeNode(concept)
        return concepts_to_lattice_nodes[concept]
