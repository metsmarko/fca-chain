from typing import List

import matplotlib.pyplot as plt

from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.visual.ConceptVisualiser import ConceptVisualiser
from fca_chain.visual.Lattice import Lattice


class ConceptLatticeVisualiser(ConceptVisualiser):

    def display_concept_lattice(self, concept_chains: List[ConceptChain]) -> None:
        self._draw_chains(concept_chains)
        plt.show()

    def save_concept_lattice_to_file(self, concept_chains: List[ConceptChain], file_name: str) -> None:
        self._draw_chains(concept_chains)
        plt.savefig(file_name)

    def _draw_chains(self, concept_chains: List[ConceptChain]) -> None:
        plt.title('Concept chain lattice')
        concepts = []
        for chain in concept_chains:
            concepts.extend(chain.get_concepts())
        lattice_nodes = Lattice.create_lattice_from_concepts(concepts).get_lattice_nodes()
        self._draw(lattice_nodes)
