from typing import List, Set, FrozenSet

from fca_chain.model.Attribute import Attribute
from fca_chain.model.Concept import Concept
from fca_chain.model.Object import Object


class LatticeNode:

    def __init__(self, concept: Concept) -> None:
        self.concept = concept
        self.node_id = concept.__hash__()
        self.next_nodes = []
        self.attributes = frozenset()
        self.objects = frozenset()

    def add_next_node(self, lattice_node: 'LatticeNode') -> None:
        self.next_nodes.append(lattice_node)

    def get_next_nodes(self) -> List['LatticeNode']:
        return self.next_nodes

    def set_attributes(self, attributes: Set[Attribute]) -> None:
        self.attributes = frozenset(attributes)

    def get_attributes(self) -> FrozenSet[Attribute]:
        return self.attributes

    def set_objects(self, objects: Set[Object]) -> None:
        self.objects = frozenset(objects)

    def get_objects(self) -> FrozenSet[Object]:
        return self.objects

    def get_concept(self) -> Concept:
        return self.concept

    def __hash__(self) -> int:
        return hash((self.attributes, self.objects))
