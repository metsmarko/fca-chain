from typing import List, Tuple, Dict

import networkx as nx
from networkx.drawing.nx_agraph import graphviz_layout

from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.visual.LatticeNode import LatticeNode


class ConceptVisualiser:
    _NODE_LABEL_OFFSET = 12

    def display_concept_lattice(self, concept_chains: List[ConceptChain]) -> None:
        pass

    def save_concept_lattice_to_file(self, concept_chains: List[ConceptChain], file_name: str) -> None:
        pass

    def _draw(self, lattice_nodes: List[LatticeNode]):
        concept_chains_lattice = nx.DiGraph()
        added_nodes = self._create_graph(concept_chains_lattice, lattice_nodes)
        self._add_labels(lattice_nodes, concept_chains_lattice, added_nodes)

    def _create_graph(self, concept_chains_lattice: nx.Graph, lattice_nodes: List[LatticeNode]) \
            -> Dict[LatticeNode, Tuple[float, float]]:
        for lattice_node in lattice_nodes:
            concept_chains_lattice.add_node(lattice_node)
            for next_node in lattice_node.get_next_nodes():
                concept_chains_lattice.add_edge(lattice_node, next_node)
        added_nodes = graphviz_layout(concept_chains_lattice, prog='dot')
        nx.draw(concept_chains_lattice, added_nodes, with_labels=False, arrows=False, node_size=60)
        return added_nodes

    def _add_labels(self, lattice_nodes, concept_chains_lattice, added_nodes) -> None:
        objects = {}
        attributes = {}
        objects_label_pos = {}
        attribute_label_pos = {}
        for lattice_node in lattice_nodes:
            x, y = added_nodes[lattice_node]
            attributes[lattice_node] = (x, y + self._NODE_LABEL_OFFSET)
            attribute_label_pos[lattice_node] = ','.join(lattice_node.get_attributes())
            objects[lattice_node] = (x, y - self._NODE_LABEL_OFFSET)
            objects_label_pos[lattice_node] = ','.join(lattice_node.get_objects())
        nx.draw_networkx_labels(concept_chains_lattice, attributes, attribute_label_pos, font_size=8)
        nx.draw_networkx_labels(concept_chains_lattice, objects, objects_label_pos, font_size=8)
