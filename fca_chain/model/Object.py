class Object:
    id = None
    name = None

    def __init__(self, id: int, name: str) -> None:
        self.id = id
        self.name = name

    def get_name(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return str(self.name)

    def __str__(self) -> str:
        return str(self.name)
