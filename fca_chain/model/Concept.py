from typing import List, FrozenSet

from fca_chain.model.Attribute import Attribute
from fca_chain.model.Object import Object


class Concept:
    extent = []
    intent = []
    _extent_ids = None
    _intent_ids = None

    def __init__(self, extent: List[Object], intent: List[Attribute]) -> None:
        super().__init__()
        self.extent = extent
        self.intent = intent

    def get_extent_ids(self) -> FrozenSet[int]:
        if self._extent_ids is None:
            self._extent_ids = frozenset([e.id for e in self.extent])
        return self._extent_ids

    def get_intent_ids(self) -> FrozenSet[int]:
        if self._intent_ids is None:
            self._intent_ids = frozenset([i.id for i in self.intent])
        return self._intent_ids

    def get_extent(self) -> List[Object]:
        return self.extent

    def get_intent(self) -> List[Attribute]:
        return self.intent

    def __str__(self) -> str:
        return "[({}) <=> ({})]".format(self.extent, self.intent)

    def __repr__(self) -> str:
        return str(self)

    def __eq__(self, o: object) -> bool:
        return self.get_extent_ids() == o.get_extent_ids() \
               and self.get_intent_ids() == o.get_intent_ids()

    def __hash__(self) -> int:
        return hash((self.get_extent_ids(), self.get_intent_ids()))
