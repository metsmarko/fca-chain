from typing import List

from numpy.core.records import ndarray
from pandas import DataFrame


class Context:
    def __init__(self, df: DataFrame) -> None:
        self._df = df
        self._order_data()
        self._object_names = list(self._df.index)
        self._attribute_names = list(self._df.columns.values)
        self._object_indices = [k for k, v in enumerate(self._object_names)]
        self._attribute_indices = [k for k, v in enumerate(self._attribute_names)]

    def get_context(self) -> DataFrame:
        return self._df

    def get_context_values(self) -> ndarray:
        return self._df.get_values()

    def get_object_name(self, i: int) -> str:
        return self._object_names[i]

    def get_attribute_name(self, i: int) -> str:
        return self._attribute_names[i]

    def get_object_indices(self) -> List[int]:
        return self._object_indices

    def get_attribute_indices(self) -> List[int]:
        return self._attribute_indices

    def get_intent(self, obj: int) -> List[int]:
        return [k for k, v in enumerate(self.get_context_values()[obj]) if v == 1]

    def get_object_names(self) -> List[str]:
        return self._object_names

    def get_attribute_names(self) -> List[str]:
        return self._attribute_names

    def _order_data(self) -> None:
        self._df = self._df.assign(temp_sum_column=self._df.sum(axis=1)).sort_values(by='temp_sum_column', ascending=False).iloc[:, :-1]
