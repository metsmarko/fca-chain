from typing import List

import pandas as pd

from fca_chain.chain.ChainGenerator import ChainGenerator
from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.clustering.Clustering import Clustering
from fca_chain.concept import AbstractConceptFinder
from fca_chain.concept.ConceptCreator import ConceptCreator
from fca_chain.concept.FrequencyBasedConceptFinder import FrequencyBasedConceptFinder
from fca_chain.concept.MinusTechniqueBasedConceptFinder import MinusTechniqueBasedConceptFinder
from fca_chain.decorator.decorators import timeit
from fca_chain.model.Context import Context


class FcaChainFinder:
    def __init__(self, context: Context) -> None:
        self.context = context

    @timeit("Total")
    def find_chain(self, amount_of_chains: int, clustering: Clustering, method: str = "mf") -> List[ConceptChain]:
        clusters = clustering.cluster_data(self.context.get_context_values(), amount_of_chains)
        cc = ConceptCreator(self.context)
        cf = self.new_concept_finder(cc, method)
        cg = ChainGenerator(self.context, amount_of_chains, cc, cf)
        return cg.generate(clusters)

    def new_concept_finder(self, cc: ConceptCreator, method: str) -> AbstractConceptFinder:
        if method == 'mf':
            return MinusTechniqueBasedConceptFinder(self.context, cc)
        elif method == 'freq':
            return FrequencyBasedConceptFinder(self.context, cc)
        else:
            raise Exception("Unknown chain finding method")

    def calculate_chains_cover(self, concept_chains: List[ConceptChain]) -> float:
        """

        :param concept_chains: list of concept chains
        :return: percentage of 1's in data table covered by the chains
        """
        context_value_copy = self.context.get_context_values().copy()
        total_sum = context_value_copy.sum()
        for chain in concept_chains:
            self._calculate_chain_cover(chain, context_value_copy)
        return 1 - context_value_copy.sum() / total_sum

    def get_new_context(self, concept_chains: List[ConceptChain]) -> pd.DataFrame:
        """

        :param concept_chains: list of concept chains
        :return: objects, attributes and their relations present in the chains
        """
        context_value_copy = self.context.get_context_values().copy()
        self.context.get_context()
        for chain in concept_chains:
            self._mark_covered_relations(chain, context_value_copy)
        context_value_copy[context_value_copy != 2] = 0
        context_value_copy[context_value_copy == 2] = 1
        new_context = pd.DataFrame(context_value_copy)
        new_context.columns = self.context.get_attribute_names()
        new_context.index = self.context.get_object_names()
        new_context = new_context[(new_context != 0).any(axis=1)]
        new_context = new_context.loc[:, (new_context != 0).any(axis=0)]
        return new_context

    def _calculate_chain_cover(self, chain, context_value_copy):
        for concept in chain.concepts:
            if len(concept.get_extent_ids()) and len(concept.get_intent_ids()):
                intents = list(concept.get_intent_ids())
                for i in concept.get_extent_ids():
                    context_value_copy[i, intents] = 0

    def _mark_covered_relations(self, chain, context_value_copy):
        for concept in chain.concepts:
            if len(concept.get_extent_ids()) and len(concept.get_intent_ids()):
                intents = list(concept.get_intent_ids())
                for i in concept.get_extent_ids():
                    context_value_copy[i, intents] = 2
