from typing import List

from fca_chain.model.Concept import Concept


class ConceptChain:
    def __init__(self) -> None:
        super().__init__()
        self.concepts = []

    def append_concepts(self, chain: List[Concept]):
        self.concepts.extend(chain)

    def append_concept(self, concept: Concept):
        self.concepts.append(concept)

    def get_concepts(self) -> List[Concept]:
        return self.concepts

    def __str__(self) -> str:
        return str(self.concepts)

    def __eq__(self, o: object) -> bool:
        return self.concepts == o.concepts

    def __hash__(self):
        return hash(frozenset(self.concepts))
