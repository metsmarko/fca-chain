from multiprocessing.dummy import Pool as ThreadPool
from typing import List

import numpy as np

from fca_chain.chain.ConceptChain import ConceptChain
from fca_chain.concept import AbstractConceptFinder
from fca_chain.concept.ConceptCreator import ConceptCreator
from fca_chain.decorator.decorators import timeit
from fca_chain.model.Concept import Concept
from fca_chain.model.Context import Context


class ChainGenerator:
    def __init__(self, context: Context, num_of_chains: int, concept_creator: ConceptCreator,
                 cf: AbstractConceptFinder) -> None:
        self.num_of_chains = num_of_chains
        self.context = context
        self.concept_creator = concept_creator
        self.cf = cf

    @timeit("Generating chains")
    def generate(self, clusters: np.ndarray) -> List[ConceptChain]:
        starting_concepts = self._get_starting_concepts(clusters)
        return ThreadPool(4).map(self._generate_chain, starting_concepts)

    def _generate_chain(self, starting_concept: Concept) -> ConceptChain:
        cc = ConceptChain()
        cc.append_concepts(self.cf.prev_concepts(starting_concept))
        cc.append_concept(starting_concept)
        cc.append_concepts(self.cf.next_concepts(starting_concept))
        return cc

    @timeit("Finding starting concepts")
    def _get_starting_concepts(self, clusters: np.ndarray) -> List[Concept]:
        starting_concepts = []
        for i in range(0, self.num_of_chains):
            object_index = self._find_first_object_index_from_cluster(clusters, i)
            starting_concepts.append(
                self.concept_creator.create_concept_from_attr(self.context.get_context_values()[object_index]))
        return starting_concepts

    def _find_first_object_index_from_cluster(self, clusters: np.ndarray, item: int) -> int:
        return np.where(clusters == item)[0][-1]
