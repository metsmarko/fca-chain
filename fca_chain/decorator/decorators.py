import functools
import time


def timeit(action):
    def decorator(method):
        @functools.wraps(method)
        def wrapper(*args, **kw):
            start = time.time()
            method_call_result = method(*args, **kw)
            print("{} {}s".format(action, format(time.time() - start, '.2f')))
            return method_call_result
        return wrapper
    return decorator
