import argparse

from fca_chain.FcaChainFinder import FcaChainFinder
from fca_chain.clustering.KModesClustering import KModesClustering
from fca_chain.data.DataReader import DataReader


def parse_args():
    parser = argparse.ArgumentParser()
    parser.add_argument("--data_file", type=str, required=True)
    parser.add_argument("--amount_of_chains", type=int, required=True)
    parser.add_argument("--method", type=str, default='mf', choices=['mf', 'freq'])
    return parser.parse_args()


if __name__ == "__main__":
    args = parse_args()

    context = DataReader.read_context_form_file(args.data_file)
    amount_of_chains = args.amount_of_chains
    method = args.method
    clustering = KModesClustering()
    fca_chain_finder = FcaChainFinder(context)

    chains = fca_chain_finder.find_chain(amount_of_chains, clustering, method)
    print("Results:")
    print("Context coverage: {}".format(format(fca_chain_finder.calculate_chains_cover(chains), '.2f')))

    i = 1
    for chain in chains:
        fca_chain_finder.get_new_context([chain]).to_csv("../out/chain{}.csv".format(i))
        i = i + 1
    fca_chain_finder.get_new_context(chains).to_csv("../out/all_chains.csv")

    concepts = set()
    for chain in chains:
        for concept in chain.get_concepts():
            concepts.add(concept)

    print("Unique concepts: ", len(concepts))
