import string

import pandas as pd

from fca_chain.model.Context import Context


class DataReader:
    @staticmethod
    def read_context_form_file(data_file: string) -> Context:
        csv = pd.read_csv(data_file, sep='\s*,\s*', engine='python')
        csv.replace(to_replace='x|X', value=1, inplace=True, regex=True)
        csv.fillna(0, inplace=True)
        return Context(csv.astype(dtype=int))
