import numpy as np
from kmodes.kmodes import KModes

from fca_chain.clustering.Clustering import Clustering


class KModesClustering(Clustering):
    def cluster_data(self, data: np.ndarray, num_of_clusters: int) -> np.ndarray:
        clustering = KModes(n_clusters=num_of_clusters).fit(data)
        return clustering.labels_
