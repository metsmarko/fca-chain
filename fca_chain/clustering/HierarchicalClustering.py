import numpy as np
from sklearn.cluster import AgglomerativeClustering

from fca_chain.clustering.Clustering import Clustering


class HierarchicalClustering(Clustering):
    def cluster_data(self, data: np.ndarray, num_of_clusters: int) -> np.ndarray:
        clustering = AgglomerativeClustering(n_clusters=num_of_clusters, linkage='ward').fit(data)
        return clustering.labels_
